package Assessment4;

public class DuplicateArray {
    public static void main(String[] args) {
        int arr[] = {20, 20, 30, 40, 50, 50, 50};
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length ; j++) {
                if (arr[i] == arr[j]) {
                    System.out.println(arr[i]);
                    count++;
                }
            }
        }
    }
}
