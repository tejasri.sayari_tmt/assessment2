package Assessment5;

import java.util.Scanner;

public class AlphabeticalOrder {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter five words: ");
        String[] s1 = new String[5];
        for (int i = 0; i< 5; i++)
            s1[i] = scan.next();
        String swap = "";
        for (int i = 0; i < s1.length; i++) {
            for (int j = i + 1; j < s1.length; j++) {
                if (s1[i].compareTo(s1[j]) > 0) {
                    swap = s1[i];
                    s1[i] = s1[j];
                    s1[j] = swap;
                }
            }
        }
        for(String word: s1)
            System.out.println(word);
    }
}
