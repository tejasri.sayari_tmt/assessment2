package Assessment5;

import java.util.Scanner;

public class RemoveTrailingSpace {
    public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            System.out.print("Enter a string: ");
            String input = scan.nextLine();
            System.out.println(input.trim());
        }
    }

