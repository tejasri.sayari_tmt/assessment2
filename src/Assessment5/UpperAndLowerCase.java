package Assessment5;

import java.util.Scanner;

public class UpperAndLowerCase {
    public static void main(String[] args) {
           Scanner scan = new Scanner(System.in);
            System.out.print("Enter a string: ");
            String input = scan.nextLine();
            System.out.println("In Upper Case: "+ input.toUpperCase());
            System.out.println("In Lower Case: "+ input.toLowerCase());
        }



    }

