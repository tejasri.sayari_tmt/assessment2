package basics;


public abstract class Area {

    public abstract void msg();
    public abstract void msg1();

    public abstract double calculateArea( int a, int b);
}
