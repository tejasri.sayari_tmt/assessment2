package polymorphisum;

public class Area {
    int n1;
    int n2;
    int n3;
    int n4;

    public Area() {

    }

    public Area(int n1, int n2, int n3, int n4) {
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        this.n4 = n4;
    }

    public double areaOfRectangular(int length, long width) {
        return length * width;
    }


    public double areaOfRectangular(int length, int width, int height) {
        return length * width * height;

    }
}
